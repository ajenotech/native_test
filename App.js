import React, { Component } from 'react';
import { Router, Stack, Scene, ActionConst } from 'react-native-router-flux';

import Index from './src/Index';
import GetName from './src/GetName';
import MyForm from './src/MyForm';

export default class App extends Component {
  render() {
      return (
        <Router>
          <Scene>
            <Scene key="root" hideNavBar={true}>
              <Scene key="index" component={Index} hideNavBar={false} title="Home Page" initial={true}/>
              <Scene key="get_name" component={GetName} hideNavBar={false} title="List Names"/>
              <Scene key="my_form" component={MyForm} hideNavBar={false} title="My Form"/>
            </Scene>
          </Scene>
        </Router>
      )
    }
}