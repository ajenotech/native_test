import React, { Component } from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { Actions } from 'react-native-router-flux';
import {Button} from './components/Button';

export default class Index extends Component {
  render() {
    return (
          <View style={styles.container}>
            
            <Button
	            title="Props Data"
	            onPress={() => Actions.get_name({propData:'Gurpreet Singh'})}
	            style={{ /* some styles for button */ }}
	            textStyle={{ /* styles for button title */ }}
	          />
             <View style={{marginBottom:10}}></View>

            <Button
              title="Input Fields"
              onPress={() => Actions.my_form()}
              style={{ backgroundColor: 'red', }}
              textStyle={{ /* styles for button title */ }}
            />


          </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1, marginTop: 30, width:'100%', paddingRight:15, paddingLeft:15
  },
});