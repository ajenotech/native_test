import React, { Component } from 'react';
import { StyleSheet, View, Text } from 'react-native';


export default class GetName extends Component {
  render() {
    return (
          <View style={styles.container}>
            <Text style={styles.text}>{this.props.propData}</Text>
            <Text style={styles.text}>{this.props.propData}</Text>
            <Text style={styles.text}>{this.props.propData}</Text>
            <Text style={styles.text}>{this.props.propData}</Text>
          </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1, marginTop: 30, width:'100%', paddingRight:15, paddingLeft:15
  },
  text:{
    fontSize:16,
    borderBottomColor: '#e2e2e2',
    borderBottomWidth: 1,
    paddingBottom:10,
    marginTop:10,
    fontWeight:'bold'
  }
});
