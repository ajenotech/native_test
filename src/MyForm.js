import React, {Component} from 'react';
import { StyleSheet, View, Text } from 'react-native';

import {connect} from "react-redux";
import {compose} from "redux";
import { Field, reduxForm } from 'redux-form'
import InputText from './components/InputText';


const styles = StyleSheet.create({
  container: {
    flex: 1, marginTop: 30, width:'100%', paddingRight:15, paddingLeft:15
  },
  label:{
    marginTop:10
  }

});

class MyForm extends Component<{}> {


  renderTextInput = (field) => {
        const {meta: {touched, error}, label, secureTextEntry, maxLength, keyboardType, placeholder, input: {onChange, ...restInput}} = field;
        return (
            <View>
              <InputText
                  onChangeText={onChange}
                  maxLength={maxLength}
                  placeholder={placeholder}
                  keyboardType={keyboardType}
                  secureTextEntry={secureTextEntry}
                  label={label}
                  {...restInput} />
              {(touched && error) && <Text style={styles.errorText}>{error}</Text>}
            </View>
        );
  }

  render() {
    return (
          <View style={styles.container}>
              
            <Text style={styles.label}>Name</Text>
            <Field
                name="name"
                component={this.renderTextInput} />

            <Text style={styles.label}>Age</Text>
            <Field
                name="age"
                component={this.renderTextInput} />

            <Text style={styles.label}>Email</Text>
            <Field
                name="email"
                component={this.renderTextInput} />

            <Text style={styles.label}>Password</Text>
            <Field
                name="password"
                secureTextEntry={true}
                component={this.renderTextInput} />

          </View>
    )
  }

}


export default  reduxForm({form: "login"})(MyForm);